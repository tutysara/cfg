" insert from clipboard --
"set clipboard+=unnamedplus
" set python path for neovim
let g:python3_host_prog = '/home/tutysara/anaconda3/envs/neovim/bin/python'

" bootstrap vimplug automatically
if empty(glob('~/.config/nvim/site/autoload'))
	silent execute "!curl -fLo ~/.config/nvim/site/autoload --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
	autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin("~/.config/nvim/plugged")
Plug 'junegunn/vim-easy-align'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'scrooloose/nerdtree'
Plug 'flazz/vim-colorschemes'
Plug 'tpope/vim-surround'
"Plug 'thaerkh/vim-workspace'
Plug 'plasticboy/vim-markdown'
Plug 'tpope/vim-obsession'

"" spacemacs like functionality
Plug 'mhinz/vim-startify'
Plug 'hecal3/vim-leader-guide'

Plug 'webdevel/tabulous' " for tab names
Plug 'majutsushi/tagbar'
Plug 'dbakker/vim-projectroot' " for guessing project root
Plug 'NLKNguyen/papercolor-theme'
Plug 'rakr/vim-one'
Plug 'pelodelfuego/vim-swoop'
Plug 'tpope/vim-surround'
Plug 'airblade/vim-rooter'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-unimpaired'


""" pythonsetup
" lsp
Plug 'autozimu/LanguageClient-neovim', {
            \ 'branch': 'next',
            \ 'do': 'bash install.sh',
            \ }
Plug 'Shougo/denite.nvim'
" (Optional) Completion integration with deoplete.
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
" (Optional) Showing function signature and inline doc.
Plug 'Shougo/echodoc.vim'
Plug 'vim-python/python-syntax', { 'for': 'python' }                " Enhanced python syntax
Plug 'Hyleus/vim-python-syntax'                                     " Enhanced Python syntax for doc string
Plug 'tmhedberg/SimpylFold'                                         " Python folding
call plug#end()

"" basic editor stuff
set mouse=a                                   " use mouse
set nocompatible                              " no need to be compatible with vi
set backspace=indent,eol,start                " make backspace delete all chars
let mapleader = "\<Space>"                    " use space as leader key
set ruler                                     " show line numeber, cursor position
set visualbell                                " Error bells are displayed visually.
set nowrap                                    " do not wrap lines by default
set backup                                    " tell vim to keep a backup
set backupdir=~/.config/nvim/backup//         " setup backup directory
set dir=~/.config/nvim/backup                 " set swap directory
set number                                    " set number
set relativenumber
""" search
set hlsearch                                  " highlight selection
set incsearch                                 " Search as you type.
set ignorecase                                " Ignore case when searching.
set cmdheight=1                               " set cmd height to 2 to see doc
""" editing
set shiftwidth=4                              " set tab and shift width
set tabstop=4
set expandtab                                 " convert tabs to space
set hidden                                    " Required for operations modifying multiple buffers like rename.
set synmaxcol=200                             " speed up syntax highlight only first 200 cols, speed up vim
set clipboard=unnamedplus
"set clipboard=unnamed
""" emacs/mac osx like keybinding
inoremap <c-k> <c-o>D
inoremap <c-u> <c-o>d^
inoremap <c-a> <c-o>0
nnoremap <c-a> 0
inoremap <c-e> <c-o>$
nnoremap <c-e> $
inoremap <c-g> <esc>
inoremap <c-x><c-s> <c-o>:w<CR>

nnoremap <c-k> lD
""" color scheme
" true color support
"set termguicolors
set background=light
colorscheme PaperColor
let g:PaperColor_Theme_Options = {
  \   'theme': {
  \     'default': {
  \       'transparent_background': 1,
  \       'allow_italic': 1,
  \       'allow_bold': 1   
  \     }
  \   }
  \ }
" escape codes are from https://github.com/morhetz/gruvbox/issues/119
let &t_ZH="\e[3m"
let &t_ZR="\e[23m"
highlight Comment cterm=italic

"colorscheme one
"let g:one_allow_italics = 1 " I love italic for comments
let g:python_highlight_all = 1 " python syntax highlight
" do not paste from middle mouse button
map <MiddleMouse> <Nop>
imap <MiddleMouse> <Nop>
map <2-MiddleMouse> <Nop>
imap <2-MiddleMouse> <Nop>

" always switch dir to current buffer dir
"autocmd! BufEnter * lcd %:p:h

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Plugin Config

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"" nerd tree
let NERDTreeMapOpenInTab='<ENTER>'

" obsession for session management
"command! Obsession "~/.config/nvim/backup/Session.vim"


"" which key (vim leader guide)
" Define prefix dictionary
let g:lmap =  {}

" Second level dictionaries:
let g:lmap.f = { 'name' : 'File Menu' }
let g:lmap.o = { 'name' : 'Open Stuff' }

call leaderGuide#register_prefix_descriptions("<Space>", "g:lmap")
nnoremap <silent> <leader> :<c-u>LeaderGuide '<Space>'<CR>
vnoremap <silent> <leader> :<c-u>LeaderGuideVisual '<Space>'<CR>

""" rooter 
let g:rooter_change_directory_for_non_project_files = 'current'
"" lsp

let g:deoplete#enable_at_startup = 1
let g:LanguageClient_autoStart = 1 " 1 is default, if required change to 0
"let g:LanguageClient_settingsPath = 'ls-settings.json'
" use with vims formatting operator gq
set formatexpr=LanguageClient#textDocument_rangeFormatting()

let g:LanguageClient_diagnosticsEnable = 1
let g:LanguageClient_serverCommands = {
    \ 'python': ['pyls']
    \ }

nnoremap <silent> K :call LanguageClient#textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient#textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient#textDocument_rename()<CR>

"" Denite
" TODO peek at options -- use C-v
" TODO helm swoop -- use C-t, C-g
" maps for navigating entries up and downwards
call denite#custom#map('insert','<Down>','<denite:move_to_next_line>','noremap')
call denite#custom#map('insert','<C-j>','<denite:move_to_next_line>','noremap')
call denite#custom#map('insert','<C-n>','<denite:move_to_next_line>','noremap')
call denite#custom#map('insert','<Up>','<denite:move_to_previous_line>','noremap')
call denite#custom#map('insert','<C-k>','<denite:move_to_previous_line>','noremap')
call denite#custom#map('insert','<C-p>','<denite:move_to_previous_line>','noremap')

" better colors
" https://github.com/languitar/config-vim/blob/master/home/.config/nvim/init.vim
call denite#custom#option('_', 'highlight_mode_insert', 'CursorLine')
call denite#custom#option('_', 'highlight_matched_range', 'None')
call denite#custom#option('_', 'highlight_matched_char', 'None')

""" folding
set foldcolumn=3                             " show 3 columns for fold in fringe
set foldlevel=1                              " open one level of fold automatically

 " fromhttps://gist.github.com/sjl/3360978
function! MyFoldText() " {{{
    let line = getline(v:foldstart)

    let nucolwidth = &fdc + &number * &numberwidth
    let windowwidth = winwidth(0) - nucolwidth - 3
    let foldedlinecount = v:foldend - v:foldstart

    " expand tabs into spaces
    let onetab = strpart('          ', 0, &tabstop)
    let line = substitute(line, '\t', onetab, 'g')

    let line = strpart(line, 0, windowwidth - 2 -len(foldedlinecount))
    let fillcharcount = windowwidth - len(line) - len(foldedlinecount)
    return line . '…' . repeat(" ",fillcharcount) . foldedlinecount . '…' . ' '
endfunction " }}}
set foldtext=MyFoldText()

""" status line settings from https://stackoverflow.com/a/9121082/615971
set noshowmode

function! InsertStatuslineColor(mode)
  if a:mode == 'i'
    hi statusline guibg=Cyan ctermfg=6 guifg=Black ctermbg=0
  elseif a:mode == 'r'
    hi statusline guibg=Purple ctermfg=5 guifg=Black ctermbg=0
  else
    hi statusline guibg=DarkRed ctermfg=1 guifg=Black ctermbg=0
  endif
endfunction

au InsertEnter * call InsertStatuslineColor(v:insertmode)
au InsertLeave * hi statusline guibg=DarkGrey ctermfg=254 guifg=White ctermbg=24

" default the statusline to green when entering Vim
"hi statusline guibg=DarkGrey ctermfg=8 guifg=White ctermbg=15
hi statusline guibg=DarkGrey ctermfg=254 guifg=White ctermbg=24

" Formats the statusline
set statusline=%f                           " file name
set statusline+=[%{strlen(&fenc)?&fenc:'none'}, "file encoding
set statusline+=%{&ff}] "file format
set statusline+=%y      "filetype
set statusline+=%h      "help file flag
set statusline+=%m      "modified flag
set statusline+=%r      "read only flag

" Puts in the current git status
"    if count(g:pathogen_disabled, 'Fugitive') < 1   
"        set statusline+=%{fugitive#statusline()}
"    endif

set statusline+=\ %=                        " align left
set statusline+=Line:%l/%L[%p%%]            " line X of Y [percent of file]
set statusline+=\ Col:%c                    " current column
set statusline+=\ Buf:%n                    " Buffer number
"set statusline+=\ [%b][0x%B]\               " ASCII and byte code under cursor

""" TODO
" italics in tmux
" completion tweak, which source to consult first, first lsp, then others
" folding by syntax
" movement, jumps, vim-unimpaired
" docs, how it is used
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Functions

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

function! ToggleExplorerAtRoot()
	if exists(':ProjectRootExe')
		exe "ProjectRootExe NERDTreeToggle"
	else
		exe "NERDTreeFind"
	endif
endfunction

function! ToggleStatusLine()
	if &laststatus == 2
		exec('set laststatus=0')
	else
		exec('set laststatus=2')
	endif
endfunction

function! ToggleTabLine()
	if &showtabline == 2
		exec('set showtabline=0')
	else
		exec('set showtabline=2')
	endif
endfunction

function! ToggleBackgroundColor()
	if &background== "dark"
		exec('set background=light')
	else
		exec('set background=dark')
	endif
endfunction

function! ToggleSignColumn()
	if &signcolumn== "no"
		exec('set signcolumn=auto')
	else
		exec('set signcolumn=no')
	endif
endfunction

function! ToggleFoldColumn()
	if &foldcolumn== 0
		exec('set foldcolumn=3')
	else
		exec('set foldcolumn=0')
	endif
endfunction


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Key bindings

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" spacemacs like keybinding
"" buffer
nmap <LEADER><TAB> <C-^>
nmap <LEADER>bb :Denite buffer<CR>
nmap <LEADER>bd :bdelete<CR>
nmap <LEADER>bk :bdelete
nmap <LEADER>bn :bn<CR>
nmap <LEADER>bp :bp<CR>
nmap <LEADER>bR :e! %<CR>
nmap <LEADER>bh :Startify<CR>
nmap <LEADER>bw :setlocal readonly!<CR>

"" file
nmap <LEADER>ff :Denite buffer file/rec<CR>
nmap <LEADER>fr :CtrlPMRU<CR>
nmap <LEADER>fs :w<CR>
nmap <LEADER>fS :wa<CR>
" there is a better solution at -- https://superuser.com/questions/195022/vim-how-to-synchronize-nerdtree-with-current-opened-tab-file-path
nmap <LEADER>ft :ProjectRootExe NERDTreeFind<CR>
nmap <LEADER>fT :NERDTreeToggle<CR>
nmap <LEADER>fy :echom expand("%:p")<CR>
nmap <LEADER>fed :e $MYVIMRC<CR>
nmap <LEADER>feR :so $MYVIMRC<CR>
nnoremap <EXPR> <LEADER>fe ":edit '.projectroot#guess().'/"

"" window
nmap <LEADER>w- :sp<CR>
nmap <LEADER>w/ :vsp<CR>
nmap <LEADER>w= <C-W>=
nmap <LEADER>wc :hide<CR>
nmap <LEADER>wh <C-W>h
nmap <LEADER>wj <C-W>j
nmap <LEADER>wk <C-W>k
nmap <LEADER>wl <C-W>l
nmap <LEADER>wm :only<CR>
nmap <LEADER>ws <C-W>s
nmap <LEADER>wv <C-W>v
nmap <LEADER>ww <C-W><C-W>

"" project
nmap <LEADER>pt :call ToggleExplorerAtRoot()<CR>

"" Toggles
nnoremap <LEADER>tb :call ToggleBackgroundColor()<CR>
nnoremap <LEADER>tf :call ToggleFoldColumn()<CR>
nnoremap <LEADER>th :set hlsearch!<CR>
nnoremap <LEADER>tl :call ToggleStatusLine()<CR>
nnoremap <LEADER>tnn :setlocal number!<CR>
nnoremap <LEADER>thh :setlocal cursorline!<CR>
nnoremap <LEADER>tnr :setlocal relativenumber!<CR>
nnoremap <LEADER>tt :TagbarToggle<CR>
nnoremap <LEADER>ts :call ToggleSignColumn()<CR>

"" Restore old workspace
nnoremap <LEADER>rw :source expand("~/.config/nvim/backup/Session.vim")<CR>


" language client, source
map <leader>lh :call LanguageClient_textDocument_hover()<CR>
map <leader>ld :call LanguageClient_textDocument_definition()<CR>
map <leader>lr :call LanguageClient_textDocument_rename()<CR>
map <leader>ls :Denite documentSymbol<CR>
map <leader>le :Denite references<CR>
map <leader>lw :Denite workspaceSymbol<CR>
nmap <leader>lf :call LanguageClient_textDocument_formatting()<CR>
vmap <leader>lf :call LanguageClient_textDocument_rangeFormatting()<CR>
map <leader>la :Denite codeAction<CR>
map <LEADER>lc :sign unplace *<CR>

" M-x or <space><space>
map <leader><space> :Denite command<CR>
" inoremap <c-x><c-s>

"" search
nmap <Leader>ss :Denite line<CR>
vmap <Leader>ss :Denite line<CR>
nmap <Leader>sS :call SwoopMulti()<CR>
vmap <Leader>sS :call SwoopMultiSelection()<CR>

"" exit out
nmap <Leader>qq :q<CR>
nmap <Leader>qQ :q!<CR>
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Autoload init.vim

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" autoload .vimrc after a change --http://www.bestofvim.com/tip/auto-reload-your-vimrc/
augroup reload_vimrc "{
	autocmd!
	autocmd BufWritePost $MYVIMRC source $MYVIMRC
augroup END "}
